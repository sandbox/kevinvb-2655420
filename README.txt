This module provides Wideo handler for video embed field module
now with this module you can add embed videos from http://www.wideo.co
to your website.

INSTALLATION
------------

This module requires the Video embed field module
Download and install this module like all other module

1. Using drush:
drush dl video_embed_wideo
drush en video_embed_wideo

2. Download:
DRUPAL HTTP URL comes here after full project is approved
Install module in module list.

USAGE
-------
Add a new video embed field to a entity of your liking.
Enable Wideo as provider in Field Settings.

Paste your embed url from Wideo in the Video field.
Example: http://wideo.co/embed/<Video ID>

You do not need to copy or remove the width and height attributes from Wideo.co
The Video embed Field will handle the dimensions.
